﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace ConsoleApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Form6 admin = new Form6();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void guardar_Click(object sender, EventArgs e)
        {

            if (p_codigo.Text != "" || p_saldo.Text != "" || p_tipo.SelectedItem.ToString() != "CAP" || p_tipo.SelectedItem.ToString() != "CAR" || p_fecha.Text != "")
            {
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_CUENTAS_CREATE @CODIGO ='" + p_codigo.Text + "',@SALDO='"+p_saldo.Text+"',@TIPO_CUENTA='"+p_tipo.SelectedItem.ToString()+"',@FECHA='"+p_fecha.Text+"'";
                cmd.ExecuteNonQuery();

                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");
                p_codigo.Text = "";
                p_saldo.Text = "";
                p_tipo.Text = "";
                p_fecha.Text = "";
                guardar.Text = "";

            }

            else
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los campos de la Cuenta!");

        }

        private void buscar_Click(object sender, EventArgs e)
        {
            if (p_cuenta.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_CUENTAS_READ @CODIGO ='"+p_cuenta.Text+"'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());


                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Empledo No Existe.");
                    p_cuenta.Text = "";
                }

                else
                {
                    dt.Columns["no_cuenta"].ColumnName = "Numero Cuenta";
                    dt.Columns["saldo"].ColumnName = "Saldo";
                    dt.Columns["antiguedad"].ColumnName = "Antiguedad";
                    dt.Columns["fecha_aper"].ColumnName = "Fecha Apertura";
                    dt.Columns["tipo_cuenta"].ColumnName = "Tipo Cuenta";
                    dt.Columns["codigo_emp"].ColumnName = "Codigo Empleado";
                    dataGridView1.DataSource = dt;
                    p_cuenta.Text = "";
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un numero de cuenta");
        }

        private void eliminar_Click(object sender, EventArgs e)
        {
            if (p_cuenta.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_CUENTAS_READ @CODIGO ='" + p_cuenta.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 1)
                {
                    ConsoleApp1.AseConnection.messageBox("Registro Eliminado Exitosamente!");
                    cmd.CommandText = "execute SP_CUENTAS_DELETE @CODIGO ='" + p_cuenta.Text + "'"; ;
                    cmd.ExecuteNonQuery();
                    p_cuenta.Text = "";
                }
                else
                {
                    ConsoleApp1.AseConnection.messageBox("Registro No Existe!");
                    p_cuenta.Text = "";
                }
            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los Campos!");
            }
        }

        private void modificar_Click(object sender, EventArgs e)
        {
            if (p_cuenta.Text != "" || pn_saldo.Text != "" || pn_fecha.Text != "")
            {
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_CUENTAS_UPDATE @CODIGO ='" + p_cuenta.Text + "',@SALDO ='" + pn_saldo.Text + "',@FECHA ='" + pn_fecha.Text + "'";
                cmd.ExecuteNonQuery();
                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");

                p_tipo.Enabled = false;

                p_cuenta.Enabled = true;
                p_cuenta.Text = "";
                pn_saldo.Text = "";
                pn_fecha.Text = "";
                modificar.Enabled = false;
            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los Campos!");
            }
        }

        private void done_Click(object sender, EventArgs e)
        {
            if (p_cuenta.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_CUENTAS_READ @CODIGO ='"+p_cuenta.Text+"'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Cuenta No Existe.");
                    p_cuenta.Text = "";
                }
                else
                {
                    pn_fecha.Text = dt.Rows[0]["antiguedad"].ToString();
                    pn_saldo.Text = dt.Rows[0]["saldo"].ToString();
                    p_cuenta.Text = dt.Rows[0]["no_cuenta"].ToString();
                    pn_fecha.Enabled = true;
                    pn_saldo.Enabled = true;
                    modificar.Enabled = true;
                    p_cuenta.Enabled = false;
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Ingrese un Numero de Cuenta!");
        }
    }
}
