﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace ConsoleApp1
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "" || p_monto.Text != "" || p_tasa.SelectedItem != null || p_tasa.SelectedItem.ToString()!= "Automatico" || p_tasa.SelectedItem.ToString() != "Fiducario" || p_saldo.Text != "" || p_periodo.Text != "")
            {
                string tata = "";
                if(p_tasa.SelectedItem.ToString()== "Automatico")
                {
                    tata = "10";
                }
                else if(p_tasa.SelectedItem.ToString() == "Fiducario")
                {
                    tata = "15";
                }
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_PRESTAMOS_CREATE @CODIGO ='" + p_codigo.Text + "',@TASA='" + tata + "',@SALDO='" + p_saldo.Text + "',@MONTO='" + p_monto.Text + "',@PERIODO='"+p_periodo.Text+"'";
                cmd.ExecuteNonQuery();

                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");
                p_codigo.Text = "";
                p_monto.Text = "";
                p_saldo.Text = "";
                p_periodo.Text = "";
                p_tasa.Text = "";
                button1.Text = "";

            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los campos de la Cuenta!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (p_prestamo.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_PRESTAMOS_READ @CODIGO ='" + p_prestamo.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());


                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Prestamo No Existe.");
                    p_prestamo.Text = "";
                }

                else
                {
                    dt.Columns["no_prestamo"].ColumnName = "Numero Prestamo";
                    dt.Columns["saldo_pr"].ColumnName = "Saldo";
                    dt.Columns["tasa_int"].ColumnName = "Tasa";
                    dt.Columns["fecha_pr"].ColumnName = "Fecha";
                    dt.Columns["monto_pr"].ColumnName = "Monto";
                    dt.Columns["cuota"].ColumnName = "Cuotas";
                    dt.Columns["periodos"].ColumnName = "Periodos";
                    dt.Columns["numerop"].ColumnName = "Numero";
                    dataGridView1.DataSource = dt;
                    p_prestamo.Text = "";
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un codigo de Prestamo");
        }

        private void eliminar_Click(object sender, EventArgs e)
        {
            if (p_prestamo.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_PRESTAMOS_READ @CODIGO ='" + p_prestamo.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 1)
                {
                    ConsoleApp1.AseConnection.messageBox("Registro Eliminado Exitosamente!");
                    cmd.CommandText = "execute SP_PRESTAMOS_DELETE @CODIGO ='" + p_prestamo.Text + "'"; ;
                    cmd.ExecuteNonQuery();
                    p_prestamo.Text = "";
                }
                else
                {
                    ConsoleApp1.AseConnection.messageBox("Prestamo No Existe!");
                    p_prestamo.Text = "";
                }
            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Ingrese otro Prestamo!");
            }
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Form6 admin = new Form6();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void modificar_Click(object sender, EventArgs e)
        {
            if (p_prestamo.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_PRESTAMOS_READ @CODIGO ='" + p_prestamo.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Prestamo No Existe.");
                    p_prestamo.Text = "";
                }
                else
                {
                    p_codigo.Text = dt.Rows[0]["codigo_emp"].ToString();
                    p_monto.Text = dt.Rows[0]["monto"].ToString();
                    pp_tasa.Text = dt.Rows[0]["tasa_int"].ToString();
                    p_saldo.Text = dt.Rows[0]["saldo_pr"].ToString();
                    p_periodo.Text = dt.Rows[0]["periodos"].ToString();
                    p_monto.Enabled = true;
                    p_tasa.Enabled = true;
                    p_saldo.Enabled = true;
                    p_periodo.Enabled = true;
                    done.Enabled = true;
                    p_codigo.Enabled = false;
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Ingrese un Numero de Prestamo!");
        }

        private void done_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "" || p_monto.Text != "" || p_tasa.SelectedText.ToString() != "Automatico" || p_tasa.SelectedText.ToString() != null || p_tasa.SelectedItem.ToString() != "Fiducario" || p_saldo.Text != "" || p_periodo.Text != "")
            {
                string tata = "";
                if (p_tasa.SelectedItem.ToString() == "Automatico")
                {
                    tata = "10";
                }
                else if (p_tasa.SelectedItem.ToString() == "Fiducario")
                {
                    tata = "15";
                }
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_PRESTAMOS_UPDATE @CODIGO ='" + p_codigo.Text + "',@TASA='" + tata + "',@SALDO='" + p_saldo.Text + "',@MONTO='" + p_monto.Text + "',@PERIODO='" + p_periodo.Text + "'";
                cmd.ExecuteNonQuery();
                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");


                p_codigo.Enabled = true;
                p_codigo.Text = "";
                p_monto.Text = "";
                p_saldo.Text = "";
                p_periodo.Text = "";
                done.Enabled = false;
            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los Campos!");
            }
        }
    }
}
