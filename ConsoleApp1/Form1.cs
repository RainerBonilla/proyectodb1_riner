﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace ConsoleApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Form6 admin = new Form6();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void guardar_Click_1(object sender, EventArgs e)
        {
            if (p_nombre.Text != "" || s_nombre.Text != "" || p_apellido.Text != "" || s_apellido.Text != "" || fecha_na.Text != "" || p_calle.Text != "" || p_avenida.Text != "" || p_casa.Text !="" || p_ciudad.Text != "" || p_departamento.Text != "" || p_referencia.Text != "")
            {

                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_EMPLEADOS_CREATE @FECHA_NA = '"+fecha_na.Text+"', @PRIMER_NOMBRE = '"+p_nombre.Text+"', @P_SEGUN_NOMBRE = '"+s_nombre.Text+"', @P_PRIMER_APELLIDO = '"+p_apellido.Text+"', @P_SEGUN_APELLIDO = '"+s_apellido.Text+"', @P_CALLE = '"+p_calle.Text+"', @P_AVENIDA = '"+p_avenida.Text+"', @P_NO_CASA = '"+p_casa.Text+"', @P_CIUDAD = '"+p_ciudad.Text+"', @P_DEPARTAMENTO = '"+p_departamento.Text+"', @P_REFERENCIA = '"+p_referencia.Text+"'";
                cmd.ExecuteNonQuery();

                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");
                p_nombre.Text = "";
                s_nombre.Text = "";
                p_apellido.Text = "";
                s_apellido.Text = "";
                fecha_na.Text = "";
                p_calle.Text = "";
                p_casa.Text = "";
                p_departamento.Text = "";
                p_ciudad.Text = "";
                p_referencia.Text = "";
                p_avenida.Text = "";

            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los campos de Empleado!");
        }

        private void buscar_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_EMPLEADOS_READ @CODIGO_EMP='"+p_codigo.Text+"'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());


                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Empledo No Existe.");
                    p_codigo.Text = "";
                }

                else
                {
                    int x2;
                    cmd.CommandText = "execute SP_CORREOS_READ @CORREO ='"+p_codigo.Text+"'";
                    cmd.ExecuteNonQuery();
                    DataTable dt2 = new DataTable();
                    OdbcDataAdapter da2 = new OdbcDataAdapter(cmd);
                    da2.Fill(dt2);
                    x2 = Convert.ToInt32(dt2.Rows.Count.ToString());

                    if (x2 != 0)
                    {
                        dt2.Columns["correo"].ColumnName = "Correo Electronico";
                        dt2.Columns["codigo_emp"].ColumnName = "Codigo Empleado";
                        dataGridView2.DataSource = dt2;
                    }

                    int x3;
                    cmd.CommandText = "execute SP_TELEFONOS_READ @TELEFONO ='"+p_codigo.Text+"'";
                    cmd.ExecuteNonQuery();
                    DataTable dt3 = new DataTable();
                    OdbcDataAdapter da3 = new OdbcDataAdapter(cmd);
                    da3.Fill(dt3);
                    x3 = Convert.ToInt32(dt3.Rows.Count.ToString());

                    if (x3 != 0)
                    {
                        dt3.Columns["telefono"].ColumnName = "Telefono";
                        dt3.Columns["codigo_emp"].ColumnName = "Codigo Empleado";
                        dataGridView3.DataSource = dt3;
                    }

                    dt.Columns["codigo_emp"].ColumnName = "Codigo Empleado";
                    dt.Columns["primer_nombre"].ColumnName = "Primer Nombre";
                    dt.Columns["segun_nombre"].ColumnName = "Segundo Nombre";
                    dt.Columns["primer_apellido"].ColumnName = "Primer Apellido";
                    dt.Columns["segun_apellido"].ColumnName = "Segundo Apellido";
                    dt.Columns["ciudad"].ColumnName = "Ciudad";
                    dt.Columns["calle"].ColumnName = "Calle";
                    dt.Columns["avenida"].ColumnName = "Avenida";
                    dt.Columns["no_casa"].ColumnName = "#Casa";
                    dt.Columns["departamento"].ColumnName = "Departamento";
                    dt.Columns["referencia"].ColumnName = "Referencia";
                    dt.Columns["fecha_inicio"].ColumnName = "Fecha Inicio";
                    dt.Columns["fecha_naci"].ColumnName = "Fecha Nacimiento";
                    dataGridView1.DataSource = dt;
                    p_codigo.Text = "";
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un valor de Empleado!");

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void eliminar_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_EMPLEADOS_READ @CODIGO_EMP ='"+p_codigo.Text+"'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 1)
                {
                    ConsoleApp1.AseConnection.messageBox("Registro Eliminado Exitosamente!");
                    cmd.CommandText = "execute SP_EMPLEADOS_DELETE @CODIGO_EMP ='"+ p_codigo.Text+"'";
                    cmd.ExecuteNonQuery();
                    p_codigo.Text = "";

                }
                else
                {
                    ConsoleApp1.AseConnection.messageBox("Registro no Existe!");
                    p_codigo.Text = "";
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un codigo de Empleado!");
        }

        private void actualizar_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_EMPLEADOS_READ @CODIGO_EMP ='"+p_codigo.Text+"'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Empledo No Existe.");
                    p_codigo.Text = "";
                }
                else
                {
                    p_nombre.Text = dt.Rows[0]["primer_nombre"].ToString();
                    s_nombre.Text = dt.Rows[0]["segun_nombre"].ToString();
                    p_apellido.Text = dt.Rows[0]["primer_apellido"].ToString();
                    s_apellido.Text = dt.Rows[0]["segun_apellido"].ToString();
                    p_ciudad.Text = dt.Rows[0]["ciudad"].ToString();
                    p_calle.Text = dt.Rows[0]["calle"].ToString();
                    p_avenida.Text = dt.Rows[0]["avenida"].ToString();
                    p_casa.Text = dt.Rows[0]["no_casa"].ToString();
                    p_departamento.Text = dt.Rows[0]["departamento"].ToString();
                    p_referencia.Text = dt.Rows[0]["referencia"].ToString();

                    p_nombre.Enabled = true;
                    s_nombre.Enabled = true;
                    p_apellido.Enabled = true;
                    s_apellido.Enabled = true;
                    p_ciudad.Enabled = true;
                    p_calle.Enabled = true;
                    p_avenida.Enabled = true;
                    p_casa.Enabled = true;
                    p_departamento.Enabled = true;
                    p_referencia.Enabled = true;
                    done.Enabled = true;
                    p_codigo.Enabled = false;

                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un codigo de Empleado!");
        }

        private void done_Click(object sender, EventArgs e)
        {
            OdbcConnection con = ConsoleApp1.AseConnection.Connect();
            OdbcCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "execute SP_EMPLEADOS_UPDATE @CODIGO_EMP ='" + p_codigo.Text + "',@PRIMER_NOMBRE ='" + p_nombre.Text + "', @P_SEGUN_NOMBRE ='" +s_nombre.Text + "',@P_PRIMER_APELLIDO ='" + p_apellido.Text + "',@P_SEGUN_APELLIDO ='" + s_apellido.Text + "',@P_CALLE ='" + p_calle.Text + "',@P_AVENIDA ='" + p_avenida.Text + "',@P_NO_CASA ='" + p_casa + "',@P_CIUDAD ='" +p_ciudad.Text + "',@P_DEPARTAMENTO ='" + p_departamento.Text + "',@P_REFERENCIA ='" + p_referencia.Text + "'";
            cmd.ExecuteNonQuery();

            ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");
            p_nombre.Enabled = false;
            s_nombre.Enabled = false;
            p_apellido.Enabled = false;
            s_apellido.Enabled = false;
            p_ciudad.Enabled = false;
            p_calle.Enabled = false;
            p_avenida.Enabled = false;
            p_casa.Enabled = false;
            p_departamento.Enabled = false;
            p_referencia.Enabled = false;
            done.Enabled = false;
            fecha_na.Enabled = false;
            p_codigo.Enabled = true;
            p_codigo.Text = "";
        }

        private void ciudad_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int x;
            OdbcConnection con = ConsoleApp1.AseConnection.Connect();
            OdbcCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = "SELECT EMPLEADOS.codigo_emp, EMPLEADOS.fecha_inicio, EMPLEADOS.fecha_naci, EMPLEADOS.primer_nombre, EMPLEADOS.segun_nombre, EMPLEADOS.primer_apellido, EMPLEADOS.segun_apellido, EMPLEADOS.calle, EMPLEADOS.avenida, EMPLEADOS.no_casa, EMPLEADOS.ciudad, EMPLEADOS.departamento, EMPLEADOS.referencia, EMPLEADOS.numeros FROM EMPLEADOS";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            OdbcDataAdapter da = new OdbcDataAdapter(cmd);
            da.Fill(dt);
            x = Convert.ToInt32(dt.Rows.Count.ToString());


            if (x == 0)
            {
                ConsoleApp1.AseConnection.messageBox("Empledo No Existe.");
                p_codigo.Text = "";
            }
            else
            {
                dt.Columns["codigo_emp"].ColumnName = "Codigo Empleado";
                dt.Columns["primer_nombre"].ColumnName = "Primer Nombre";
                dt.Columns["segun_nombre"].ColumnName = "Segundo Nombre";
                dt.Columns["primer_apellido"].ColumnName = "Primer Apellido";
                dt.Columns["segun_apellido"].ColumnName = "Segundo Apellido";
                dt.Columns["ciudad"].ColumnName = "Ciudad";
                dt.Columns["calle"].ColumnName = "Calle";
                dt.Columns["avenida"].ColumnName = "Avenida";
                dt.Columns["no_casa"].ColumnName = "#Casa";
                dt.Columns["departamento"].ColumnName = "Departamento";
                dt.Columns["referencia"].ColumnName = "Referencia";
                dt.Columns["fecha_inicio"].ColumnName = "Fecha Inicio";
                dt.Columns["fecha_naci"].ColumnName = "Fecha Nacimiento";
                dataGridView1.DataSource = dt;
                p_codigo.Text = "";
            }
        }
    }
}
