﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace ConsoleApp1
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void empleado_Click(object sender, EventArgs e)
        {
            Form1 admin = new Form1();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void cuenta_Click(object sender, EventArgs e)
        {
            Form2 admin = new Form2();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void abono_Click(object sender, EventArgs e)
        {
            Form3 admin = new Form3();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void prestamo_Click(object sender, EventArgs e)
        {
            Form4 admin = new Form4();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void pago_Click(object sender, EventArgs e)
        {
            Form5 admin = new Form5();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Form7 admin = new Form7();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }
    }
}
