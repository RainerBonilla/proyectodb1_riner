﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace ConsoleApp1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Form6 admin = new Form6();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void guardar_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "" || p_monto.Text != "" || p_tipo.SelectedItem.ToString() != "CAP" || p_tipo.SelectedItem.ToString() != "CAR" || p_coment.Text != "")
            {
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_ABONOS_CREATE @CODIGO ='" + p_codigo.Text + "',@MONTO='" + p_monto.Text + "',@COMENTARIO='" + p_coment.Text + "',@TIPO='" + p_tipo.SelectedItem.ToString() + "'";
                cmd.ExecuteNonQuery();

                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");
                p_codigo.Text = "";
                p_monto.Text = "";
                p_tipo.Text = "";
                p_coment.Text = "";
                guardar.Text = "";

            }

            else
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los campos de la Cuenta!");
        }

        private void buscar_Click(object sender, EventArgs e)
        {
            if (p_abono.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_ABONOS_READ @CODIGO ='" + p_abono.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());


                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Abono No Existe.");
                    p_abono.Text = "";
                }

                else
                {
                    dt.Columns["no_abono"].ColumnName = "Numero Abono";
                    dt.Columns["monto_ab"].ColumnName = "Monto";
                    dt.Columns["fecha_ab"].ColumnName = "Fecha Abono";
                    dt.Columns["comentario"].ColumnName = "Comentario";
                    dt.Columns["no_cuenta"].ColumnName = "Cuenta";
                    dt.Columns["codigo_emp"].ColumnName = "Codigo Empleado";
                    dt.Columns["numeroab"].ColumnName = "Numero";
                    dataGridView1.DataSource = dt;
                    p_abono.Text = "";
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un codigo de abono");
        }

        private void eliminar_Click(object sender, EventArgs e)
        {
            if (p_abono.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_ABONOS_READ @CODIGO ='" + p_abono.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 1)
                {
                    ConsoleApp1.AseConnection.messageBox("Registro Eliminado Exitosamente!");
                    cmd.CommandText = "execute SP_ABONOS_DELETE @CODIGO ='" + p_abono.Text + "'"; ;
                    cmd.ExecuteNonQuery();
                    p_abono.Text = "";
                }
                else
                {
                    ConsoleApp1.AseConnection.messageBox("Abono No Existe!");
                    p_abono.Text = "";
                }
            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Ingrese otro Abono!");
            }
        }

        private void modificar_Click(object sender, EventArgs e)
        {
            if (p_abono.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_ABONOS_READ @CODIGO ='" + p_abono.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("abono No Existe.");
                    p_abono.Text = "";
                }
                else
                {
                    p_codigo.Text = dt.Rows[0]["codigo_emp"].ToString();
                    p_monto.Text = dt.Rows[0]["monto"].ToString();
                    p_coment.Text = dt.Rows[0]["comentario"].ToString();
                    p_monto.Enabled = true;
                    p_coment.Enabled = true;
                    done.Enabled = true;
                    p_codigo.Enabled = false;
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Ingrese un Numero de abono!");
        }

        private void done_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "" || p_monto.Text != "" || p_coment.Text != "")
            {
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_ABONOS_UPDATE @CODIGO ='" + p_codigo.Text + "',@MONTO ='" + p_monto.Text + "',@COMENTARIO ='" + p_coment.Text + "'";
                cmd.ExecuteNonQuery();
                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");

                p_tipo.Enabled = false;

                p_codigo.Enabled = true;
                p_codigo.Text = "";
                p_monto.Text = "";
                p_coment.Text = "";
                done.Enabled = false;
            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los Campos!");
            }
        }
    }
    
}
