﻿namespace ConsoleApp1
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ADMINISTRACION = new System.Windows.Forms.Label();
            this.abonos = new System.Windows.Forms.Label();
            this.cuentas = new System.Windows.Forms.Label();
            this.empleados = new System.Windows.Forms.Label();
            this.pagos = new System.Windows.Forms.Label();
            this.prestamos = new System.Windows.Forms.Label();
            this.empleado = new System.Windows.Forms.Button();
            this.cuenta = new System.Windows.Forms.Button();
            this.abono = new System.Windows.Forms.Button();
            this.prestamo = new System.Windows.Forms.Button();
            this.pago = new System.Windows.Forms.Button();
            this.cerrar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ADMINISTRACION
            // 
            this.ADMINISTRACION.AutoSize = true;
            this.ADMINISTRACION.Location = new System.Drawing.Point(190, 30);
            this.ADMINISTRACION.Name = "ADMINISTRACION";
            this.ADMINISTRACION.Size = new System.Drawing.Size(100, 13);
            this.ADMINISTRACION.TabIndex = 0;
            this.ADMINISTRACION.Text = "ADMINISTRACION";
            // 
            // abonos
            // 
            this.abonos.AutoSize = true;
            this.abonos.Location = new System.Drawing.Point(304, 97);
            this.abonos.Name = "abonos";
            this.abonos.Size = new System.Drawing.Size(42, 13);
            this.abonos.TabIndex = 1;
            this.abonos.Text = "abonos";
            // 
            // cuentas
            // 
            this.cuentas.AutoSize = true;
            this.cuentas.Location = new System.Drawing.Point(199, 97);
            this.cuentas.Name = "cuentas";
            this.cuentas.Size = new System.Drawing.Size(45, 13);
            this.cuentas.TabIndex = 2;
            this.cuentas.Text = "cuentas";
            // 
            // empleados
            // 
            this.empleados.AutoSize = true;
            this.empleados.Location = new System.Drawing.Point(88, 97);
            this.empleados.Name = "empleados";
            this.empleados.Size = new System.Drawing.Size(58, 13);
            this.empleados.TabIndex = 3;
            this.empleados.Text = "empleados";
            // 
            // pagos
            // 
            this.pagos.AutoSize = true;
            this.pagos.Location = new System.Drawing.Point(208, 198);
            this.pagos.Name = "pagos";
            this.pagos.Size = new System.Drawing.Size(36, 13);
            this.pagos.TabIndex = 4;
            this.pagos.Text = "pagos";
            // 
            // prestamos
            // 
            this.prestamos.AutoSize = true;
            this.prestamos.Location = new System.Drawing.Point(88, 198);
            this.prestamos.Name = "prestamos";
            this.prestamos.Size = new System.Drawing.Size(55, 13);
            this.prestamos.TabIndex = 5;
            this.prestamos.Text = "prestamos";
            // 
            // empleado
            // 
            this.empleado.Location = new System.Drawing.Point(71, 143);
            this.empleado.Name = "empleado";
            this.empleado.Size = new System.Drawing.Size(75, 23);
            this.empleado.TabIndex = 6;
            this.empleado.Text = "entrar";
            this.empleado.UseVisualStyleBackColor = true;
            this.empleado.Click += new System.EventHandler(this.empleado_Click);
            // 
            // cuenta
            // 
            this.cuenta.Location = new System.Drawing.Point(178, 143);
            this.cuenta.Name = "cuenta";
            this.cuenta.Size = new System.Drawing.Size(75, 23);
            this.cuenta.TabIndex = 7;
            this.cuenta.Text = "entrar";
            this.cuenta.UseVisualStyleBackColor = true;
            this.cuenta.Click += new System.EventHandler(this.cuenta_Click);
            // 
            // abono
            // 
            this.abono.Location = new System.Drawing.Point(288, 143);
            this.abono.Name = "abono";
            this.abono.Size = new System.Drawing.Size(75, 23);
            this.abono.TabIndex = 8;
            this.abono.Text = "entrar";
            this.abono.UseVisualStyleBackColor = true;
            this.abono.Click += new System.EventHandler(this.abono_Click);
            // 
            // prestamo
            // 
            this.prestamo.Location = new System.Drawing.Point(71, 233);
            this.prestamo.Name = "prestamo";
            this.prestamo.Size = new System.Drawing.Size(75, 23);
            this.prestamo.TabIndex = 9;
            this.prestamo.Text = "entrar";
            this.prestamo.UseVisualStyleBackColor = true;
            this.prestamo.Click += new System.EventHandler(this.prestamo_Click);
            // 
            // pago
            // 
            this.pago.Location = new System.Drawing.Point(178, 233);
            this.pago.Name = "pago";
            this.pago.Size = new System.Drawing.Size(75, 23);
            this.pago.TabIndex = 10;
            this.pago.Text = "entrar";
            this.pago.UseVisualStyleBackColor = true;
            this.pago.Click += new System.EventHandler(this.pago_Click);
            // 
            // cerrar
            // 
            this.cerrar.Location = new System.Drawing.Point(430, 317);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(75, 23);
            this.cerrar.TabIndex = 11;
            this.cerrar.Text = "cerrar";
            this.cerrar.UseVisualStyleBackColor = true;
            this.cerrar.Click += new System.EventHandler(this.cerrar_Click);
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 352);
            this.Controls.Add(this.cerrar);
            this.Controls.Add(this.pago);
            this.Controls.Add(this.prestamo);
            this.Controls.Add(this.abono);
            this.Controls.Add(this.cuenta);
            this.Controls.Add(this.empleado);
            this.Controls.Add(this.prestamos);
            this.Controls.Add(this.pagos);
            this.Controls.Add(this.empleados);
            this.Controls.Add(this.cuentas);
            this.Controls.Add(this.abonos);
            this.Controls.Add(this.ADMINISTRACION);
            this.Name = "Form6";
            this.Text = "Form6";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ADMINISTRACION;
        private System.Windows.Forms.Label abonos;
        private System.Windows.Forms.Label cuentas;
        private System.Windows.Forms.Label empleados;
        private System.Windows.Forms.Label pagos;
        private System.Windows.Forms.Label prestamos;
        private System.Windows.Forms.Button empleado;
        private System.Windows.Forms.Button cuenta;
        private System.Windows.Forms.Button abono;
        private System.Windows.Forms.Button prestamo;
        private System.Windows.Forms.Button pago;
        private System.Windows.Forms.Button cerrar;
    }
}