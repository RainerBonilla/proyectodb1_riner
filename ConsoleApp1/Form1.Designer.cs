﻿namespace ConsoleApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EMPLEADOS = new System.Windows.Forms.Label();
            this.primer_nombre = new System.Windows.Forms.Label();
            this.segundo_nombre = new System.Windows.Forms.Label();
            this.primer_apellido = new System.Windows.Forms.Label();
            this.segundo_apellido = new System.Windows.Forms.Label();
            this.fecha_nacimiento = new System.Windows.Forms.Label();
            this.calle = new System.Windows.Forms.Label();
            this.avenida = new System.Windows.Forms.Label();
            this.casa = new System.Windows.Forms.Label();
            this.ciudad = new System.Windows.Forms.Label();
            this.departamento = new System.Windows.Forms.Label();
            this.referencia = new System.Windows.Forms.Label();
            this.p_nombre = new System.Windows.Forms.TextBox();
            this.p_calle = new System.Windows.Forms.TextBox();
            this.s_apellido = new System.Windows.Forms.TextBox();
            this.p_apellido = new System.Windows.Forms.TextBox();
            this.s_nombre = new System.Windows.Forms.TextBox();
            this.p_departamento = new System.Windows.Forms.TextBox();
            this.p_ciudad = new System.Windows.Forms.TextBox();
            this.p_avenida = new System.Windows.Forms.TextBox();
            this.p_referencia = new System.Windows.Forms.TextBox();
            this.guardar = new System.Windows.Forms.Button();
            this.cerrar = new System.Windows.Forms.Button();
            this.fecha_na = new System.Windows.Forms.TextBox();
            this.p_casa = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buscar = new System.Windows.Forms.Button();
            this.p_codigo = new System.Windows.Forms.TextBox();
            this.codigo = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.telefonos = new System.Windows.Forms.Label();
            this.correos = new System.Windows.Forms.Label();
            this.eliminar = new System.Windows.Forms.Button();
            this.actualizar = new System.Windows.Forms.Button();
            this.done = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // EMPLEADOS
            // 
            this.EMPLEADOS.AutoSize = true;
            this.EMPLEADOS.Location = new System.Drawing.Point(309, 22);
            this.EMPLEADOS.Name = "EMPLEADOS";
            this.EMPLEADOS.Size = new System.Drawing.Size(73, 13);
            this.EMPLEADOS.TabIndex = 0;
            this.EMPLEADOS.Text = "EMPLEADOS";
            this.EMPLEADOS.Click += new System.EventHandler(this.label1_Click);
            // 
            // primer_nombre
            // 
            this.primer_nombre.AutoSize = true;
            this.primer_nombre.Location = new System.Drawing.Point(37, 74);
            this.primer_nombre.Name = "primer_nombre";
            this.primer_nombre.Size = new System.Drawing.Size(76, 13);
            this.primer_nombre.TabIndex = 1;
            this.primer_nombre.Text = "primer_nombre";
            // 
            // segundo_nombre
            // 
            this.segundo_nombre.AutoSize = true;
            this.segundo_nombre.Location = new System.Drawing.Point(24, 109);
            this.segundo_nombre.Name = "segundo_nombre";
            this.segundo_nombre.Size = new System.Drawing.Size(89, 13);
            this.segundo_nombre.TabIndex = 2;
            this.segundo_nombre.Text = "segundo_nombre";
            // 
            // primer_apellido
            // 
            this.primer_apellido.AutoSize = true;
            this.primer_apellido.Location = new System.Drawing.Point(37, 140);
            this.primer_apellido.Name = "primer_apellido";
            this.primer_apellido.Size = new System.Drawing.Size(77, 13);
            this.primer_apellido.TabIndex = 3;
            this.primer_apellido.Text = "primer_apellido";
            // 
            // segundo_apellido
            // 
            this.segundo_apellido.AutoSize = true;
            this.segundo_apellido.Location = new System.Drawing.Point(23, 171);
            this.segundo_apellido.Name = "segundo_apellido";
            this.segundo_apellido.Size = new System.Drawing.Size(90, 13);
            this.segundo_apellido.TabIndex = 4;
            this.segundo_apellido.Text = "segundo_apellido";
            // 
            // fecha_nacimiento
            // 
            this.fecha_nacimiento.AutoSize = true;
            this.fecha_nacimiento.Location = new System.Drawing.Point(245, 74);
            this.fecha_nacimiento.Name = "fecha_nacimiento";
            this.fecha_nacimiento.Size = new System.Drawing.Size(91, 13);
            this.fecha_nacimiento.TabIndex = 5;
            this.fecha_nacimiento.Text = "fecha_nacimiento";
            // 
            // calle
            // 
            this.calle.AutoSize = true;
            this.calle.Location = new System.Drawing.Point(245, 109);
            this.calle.Name = "calle";
            this.calle.Size = new System.Drawing.Size(29, 13);
            this.calle.TabIndex = 6;
            this.calle.Text = "calle";
            // 
            // avenida
            // 
            this.avenida.AutoSize = true;
            this.avenida.Location = new System.Drawing.Point(245, 140);
            this.avenida.Name = "avenida";
            this.avenida.Size = new System.Drawing.Size(45, 13);
            this.avenida.TabIndex = 7;
            this.avenida.Text = "avenida";
            // 
            // casa
            // 
            this.casa.AutoSize = true;
            this.casa.Location = new System.Drawing.Point(245, 171);
            this.casa.Name = "casa";
            this.casa.Size = new System.Drawing.Size(30, 13);
            this.casa.TabIndex = 8;
            this.casa.Text = "casa";
            // 
            // ciudad
            // 
            this.ciudad.AutoSize = true;
            this.ciudad.Location = new System.Drawing.Point(546, 74);
            this.ciudad.Name = "ciudad";
            this.ciudad.Size = new System.Drawing.Size(39, 13);
            this.ciudad.TabIndex = 9;
            this.ciudad.Text = "ciudad";
            this.ciudad.Click += new System.EventHandler(this.ciudad_Click);
            // 
            // departamento
            // 
            this.departamento.AutoSize = true;
            this.departamento.Location = new System.Drawing.Point(513, 109);
            this.departamento.Name = "departamento";
            this.departamento.Size = new System.Drawing.Size(72, 13);
            this.departamento.TabIndex = 10;
            this.departamento.Text = "departamento";
            // 
            // referencia
            // 
            this.referencia.AutoSize = true;
            this.referencia.Location = new System.Drawing.Point(531, 140);
            this.referencia.Name = "referencia";
            this.referencia.Size = new System.Drawing.Size(54, 13);
            this.referencia.TabIndex = 11;
            this.referencia.Text = "referencia";
            // 
            // p_nombre
            // 
            this.p_nombre.Location = new System.Drawing.Point(119, 71);
            this.p_nombre.Name = "p_nombre";
            this.p_nombre.Size = new System.Drawing.Size(100, 20);
            this.p_nombre.TabIndex = 12;
            // 
            // p_calle
            // 
            this.p_calle.Location = new System.Drawing.Point(291, 109);
            this.p_calle.Name = "p_calle";
            this.p_calle.Size = new System.Drawing.Size(100, 20);
            this.p_calle.TabIndex = 13;
            // 
            // s_apellido
            // 
            this.s_apellido.Location = new System.Drawing.Point(120, 168);
            this.s_apellido.Name = "s_apellido";
            this.s_apellido.Size = new System.Drawing.Size(100, 20);
            this.s_apellido.TabIndex = 14;
            // 
            // p_apellido
            // 
            this.p_apellido.Location = new System.Drawing.Point(120, 140);
            this.p_apellido.Name = "p_apellido";
            this.p_apellido.Size = new System.Drawing.Size(100, 20);
            this.p_apellido.TabIndex = 15;
            // 
            // s_nombre
            // 
            this.s_nombre.Location = new System.Drawing.Point(119, 106);
            this.s_nombre.Name = "s_nombre";
            this.s_nombre.Size = new System.Drawing.Size(100, 20);
            this.s_nombre.TabIndex = 16;
            // 
            // p_departamento
            // 
            this.p_departamento.Location = new System.Drawing.Point(602, 109);
            this.p_departamento.Name = "p_departamento";
            this.p_departamento.Size = new System.Drawing.Size(100, 20);
            this.p_departamento.TabIndex = 17;
            // 
            // p_ciudad
            // 
            this.p_ciudad.Location = new System.Drawing.Point(602, 74);
            this.p_ciudad.Name = "p_ciudad";
            this.p_ciudad.Size = new System.Drawing.Size(100, 20);
            this.p_ciudad.TabIndex = 18;
            // 
            // p_avenida
            // 
            this.p_avenida.Location = new System.Drawing.Point(291, 140);
            this.p_avenida.Name = "p_avenida";
            this.p_avenida.Size = new System.Drawing.Size(100, 20);
            this.p_avenida.TabIndex = 19;
            // 
            // p_referencia
            // 
            this.p_referencia.Location = new System.Drawing.Point(602, 140);
            this.p_referencia.Name = "p_referencia";
            this.p_referencia.Size = new System.Drawing.Size(100, 20);
            this.p_referencia.TabIndex = 20;
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(616, 168);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(75, 23);
            this.guardar.TabIndex = 23;
            this.guardar.Text = "guardar";
            this.guardar.UseVisualStyleBackColor = true;
            this.guardar.Click += new System.EventHandler(this.guardar_Click_1);
            // 
            // cerrar
            // 
            this.cerrar.Location = new System.Drawing.Point(659, 497);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(75, 23);
            this.cerrar.TabIndex = 24;
            this.cerrar.Text = "cerrar";
            this.cerrar.UseVisualStyleBackColor = true;
            this.cerrar.Click += new System.EventHandler(this.cerrar_Click);
            // 
            // fecha_na
            // 
            this.fecha_na.Location = new System.Drawing.Point(342, 74);
            this.fecha_na.Name = "fecha_na";
            this.fecha_na.Size = new System.Drawing.Size(100, 20);
            this.fecha_na.TabIndex = 25;
            // 
            // p_casa
            // 
            this.p_casa.Location = new System.Drawing.Point(281, 171);
            this.p_casa.Name = "p_casa";
            this.p_casa.Size = new System.Drawing.Size(34, 20);
            this.p_casa.TabIndex = 26;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(57, 248);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(615, 131);
            this.dataGridView1.TabIndex = 27;
            // 
            // buscar
            // 
            this.buscar.Location = new System.Drawing.Point(169, 210);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(75, 23);
            this.buscar.TabIndex = 28;
            this.buscar.Text = "buscar";
            this.buscar.UseVisualStyleBackColor = true;
            this.buscar.Click += new System.EventHandler(this.buscar_Click);
            // 
            // p_codigo
            // 
            this.p_codigo.Location = new System.Drawing.Point(291, 210);
            this.p_codigo.Name = "p_codigo";
            this.p_codigo.Size = new System.Drawing.Size(100, 20);
            this.p_codigo.TabIndex = 29;
            // 
            // codigo
            // 
            this.codigo.AutoSize = true;
            this.codigo.Location = new System.Drawing.Point(250, 213);
            this.codigo.Name = "codigo";
            this.codigo.Size = new System.Drawing.Size(39, 13);
            this.codigo.TabIndex = 30;
            this.codigo.Text = "codigo";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(75, 419);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(214, 101);
            this.dataGridView2.TabIndex = 31;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(421, 419);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(214, 101);
            this.dataGridView3.TabIndex = 32;
            // 
            // telefonos
            // 
            this.telefonos.AutoSize = true;
            this.telefonos.Location = new System.Drawing.Point(495, 392);
            this.telefonos.Name = "telefonos";
            this.telefonos.Size = new System.Drawing.Size(50, 13);
            this.telefonos.TabIndex = 33;
            this.telefonos.Text = "telefonos";
            this.telefonos.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // correos
            // 
            this.correos.AutoSize = true;
            this.correos.Location = new System.Drawing.Point(153, 392);
            this.correos.Name = "correos";
            this.correos.Size = new System.Drawing.Size(42, 13);
            this.correos.TabIndex = 34;
            this.correos.Text = "correos";
            // 
            // eliminar
            // 
            this.eliminar.Location = new System.Drawing.Point(409, 210);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 23);
            this.eliminar.TabIndex = 35;
            this.eliminar.Text = "eliminar";
            this.eliminar.UseVisualStyleBackColor = true;
            this.eliminar.Click += new System.EventHandler(this.eliminar_Click);
            // 
            // actualizar
            // 
            this.actualizar.Location = new System.Drawing.Point(549, 210);
            this.actualizar.Name = "actualizar";
            this.actualizar.Size = new System.Drawing.Size(75, 23);
            this.actualizar.TabIndex = 36;
            this.actualizar.Text = "actualizar";
            this.actualizar.UseVisualStyleBackColor = true;
            this.actualizar.Click += new System.EventHandler(this.actualizar_Click);
            // 
            // done
            // 
            this.done.Enabled = false;
            this.done.Location = new System.Drawing.Point(630, 210);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(75, 23);
            this.done.TabIndex = 37;
            this.done.Text = "done";
            this.done.UseVisualStyleBackColor = true;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(312, 387);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 38;
            this.button1.Text = "mostrar todo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 532);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.done);
            this.Controls.Add(this.actualizar);
            this.Controls.Add(this.eliminar);
            this.Controls.Add(this.correos);
            this.Controls.Add(this.telefonos);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.codigo);
            this.Controls.Add(this.p_codigo);
            this.Controls.Add(this.buscar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.p_casa);
            this.Controls.Add(this.fecha_na);
            this.Controls.Add(this.cerrar);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.p_referencia);
            this.Controls.Add(this.p_avenida);
            this.Controls.Add(this.p_ciudad);
            this.Controls.Add(this.p_departamento);
            this.Controls.Add(this.s_nombre);
            this.Controls.Add(this.p_apellido);
            this.Controls.Add(this.s_apellido);
            this.Controls.Add(this.p_calle);
            this.Controls.Add(this.p_nombre);
            this.Controls.Add(this.referencia);
            this.Controls.Add(this.departamento);
            this.Controls.Add(this.ciudad);
            this.Controls.Add(this.casa);
            this.Controls.Add(this.avenida);
            this.Controls.Add(this.calle);
            this.Controls.Add(this.fecha_nacimiento);
            this.Controls.Add(this.segundo_apellido);
            this.Controls.Add(this.primer_apellido);
            this.Controls.Add(this.segundo_nombre);
            this.Controls.Add(this.primer_nombre);
            this.Controls.Add(this.EMPLEADOS);
            this.Name = "Form1";
            this.Text = "ABC";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label EMPLEADOS;
        private System.Windows.Forms.Label primer_nombre;
        private System.Windows.Forms.Label segundo_nombre;
        private System.Windows.Forms.Label primer_apellido;
        private System.Windows.Forms.Label segundo_apellido;
        private System.Windows.Forms.Label fecha_nacimiento;
        private System.Windows.Forms.Label calle;
        private System.Windows.Forms.Label avenida;
        private System.Windows.Forms.Label casa;
        private System.Windows.Forms.Label ciudad;
        private System.Windows.Forms.Label departamento;
        private System.Windows.Forms.Label referencia;
        private System.Windows.Forms.TextBox p_nombre;
        private System.Windows.Forms.TextBox p_calle;
        private System.Windows.Forms.TextBox s_apellido;
        private System.Windows.Forms.TextBox p_apellido;
        private System.Windows.Forms.TextBox s_nombre;
        private System.Windows.Forms.TextBox p_departamento;
        private System.Windows.Forms.TextBox p_ciudad;
        private System.Windows.Forms.TextBox p_avenida;
        private System.Windows.Forms.TextBox p_referencia;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.Button cerrar;
        private System.Windows.Forms.TextBox fecha_na;
        private System.Windows.Forms.TextBox p_casa;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buscar;
        private System.Windows.Forms.TextBox p_codigo;
        private System.Windows.Forms.Label codigo;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label telefonos;
        private System.Windows.Forms.Label correos;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.Button actualizar;
        private System.Windows.Forms.Button done;
        private System.Windows.Forms.Button button1;
    }
}