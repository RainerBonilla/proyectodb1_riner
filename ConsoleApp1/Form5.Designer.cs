﻿namespace ConsoleApp1
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PAGOS = new System.Windows.Forms.Label();
            this.periodos = new System.Windows.Forms.Label();
            this.interes = new System.Windows.Forms.Label();
            this.monto = new System.Windows.Forms.Label();
            this.codigo_empleado = new System.Windows.Forms.Label();
            this.numero_pago = new System.Windows.Forms.Label();
            this.numero_prestamo = new System.Windows.Forms.Label();
            this.pago = new System.Windows.Forms.Label();
            this.p_codigo = new System.Windows.Forms.TextBox();
            this.p_monto = new System.Windows.Forms.TextBox();
            this.p_interes = new System.Windows.Forms.TextBox();
            this.p_periodo = new System.Windows.Forms.TextBox();
            this.p_pago = new System.Windows.Forms.TextBox();
            this.p_prestamo = new System.Windows.Forms.TextBox();
            this.pn_pago = new System.Windows.Forms.TextBox();
            this.buscar = new System.Windows.Forms.Button();
            this.modificar = new System.Windows.Forms.Button();
            this.eliminar = new System.Windows.Forms.Button();
            this.guardar = new System.Windows.Forms.Button();
            this.done = new System.Windows.Forms.Button();
            this.cerrar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // PAGOS
            // 
            this.PAGOS.AutoSize = true;
            this.PAGOS.Location = new System.Drawing.Point(236, 18);
            this.PAGOS.Name = "PAGOS";
            this.PAGOS.Size = new System.Drawing.Size(44, 13);
            this.PAGOS.TabIndex = 0;
            this.PAGOS.Text = "PAGOS";
            // 
            // periodos
            // 
            this.periodos.AutoSize = true;
            this.periodos.Location = new System.Drawing.Point(41, 146);
            this.periodos.Name = "periodos";
            this.periodos.Size = new System.Drawing.Size(47, 13);
            this.periodos.TabIndex = 1;
            this.periodos.Text = "periodos";
            // 
            // interes
            // 
            this.interes.AutoSize = true;
            this.interes.Location = new System.Drawing.Point(41, 118);
            this.interes.Name = "interes";
            this.interes.Size = new System.Drawing.Size(38, 13);
            this.interes.TabIndex = 2;
            this.interes.Text = "interes";
            // 
            // monto
            // 
            this.monto.AutoSize = true;
            this.monto.Location = new System.Drawing.Point(41, 89);
            this.monto.Name = "monto";
            this.monto.Size = new System.Drawing.Size(36, 13);
            this.monto.TabIndex = 3;
            this.monto.Text = "monto";
            // 
            // codigo_empleado
            // 
            this.codigo_empleado.AutoSize = true;
            this.codigo_empleado.Location = new System.Drawing.Point(41, 58);
            this.codigo_empleado.Name = "codigo_empleado";
            this.codigo_empleado.Size = new System.Drawing.Size(91, 13);
            this.codigo_empleado.TabIndex = 4;
            this.codigo_empleado.Text = "codigo_empleado";
            // 
            // numero_pago
            // 
            this.numero_pago.AutoSize = true;
            this.numero_pago.Location = new System.Drawing.Point(41, 173);
            this.numero_pago.Name = "numero_pago";
            this.numero_pago.Size = new System.Drawing.Size(72, 13);
            this.numero_pago.TabIndex = 5;
            this.numero_pago.Text = "numero_pago";
            // 
            // numero_prestamo
            // 
            this.numero_prestamo.AutoSize = true;
            this.numero_prestamo.Location = new System.Drawing.Point(41, 199);
            this.numero_prestamo.Name = "numero_prestamo";
            this.numero_prestamo.Size = new System.Drawing.Size(91, 13);
            this.numero_prestamo.TabIndex = 6;
            this.numero_prestamo.Text = "numero_prestamo";
            // 
            // pago
            // 
            this.pago.AutoSize = true;
            this.pago.Location = new System.Drawing.Point(327, 55);
            this.pago.Name = "pago";
            this.pago.Size = new System.Drawing.Size(31, 13);
            this.pago.TabIndex = 7;
            this.pago.Text = "pago";
            // 
            // p_codigo
            // 
            this.p_codigo.Location = new System.Drawing.Point(138, 55);
            this.p_codigo.Name = "p_codigo";
            this.p_codigo.Size = new System.Drawing.Size(100, 20);
            this.p_codigo.TabIndex = 8;
            // 
            // p_monto
            // 
            this.p_monto.Location = new System.Drawing.Point(92, 86);
            this.p_monto.Name = "p_monto";
            this.p_monto.Size = new System.Drawing.Size(67, 20);
            this.p_monto.TabIndex = 9;
            // 
            // p_interes
            // 
            this.p_interes.Location = new System.Drawing.Point(85, 115);
            this.p_interes.Name = "p_interes";
            this.p_interes.Size = new System.Drawing.Size(47, 20);
            this.p_interes.TabIndex = 10;
            // 
            // p_periodo
            // 
            this.p_periodo.Location = new System.Drawing.Point(94, 143);
            this.p_periodo.Name = "p_periodo";
            this.p_periodo.Size = new System.Drawing.Size(38, 20);
            this.p_periodo.TabIndex = 11;
            // 
            // p_pago
            // 
            this.p_pago.Location = new System.Drawing.Point(119, 170);
            this.p_pago.Name = "p_pago";
            this.p_pago.Size = new System.Drawing.Size(34, 20);
            this.p_pago.TabIndex = 12;
            // 
            // p_prestamo
            // 
            this.p_prestamo.Location = new System.Drawing.Point(138, 196);
            this.p_prestamo.Name = "p_prestamo";
            this.p_prestamo.Size = new System.Drawing.Size(37, 20);
            this.p_prestamo.TabIndex = 13;
            // 
            // pn_pago
            // 
            this.pn_pago.Location = new System.Drawing.Point(299, 82);
            this.pn_pago.Name = "pn_pago";
            this.pn_pago.Size = new System.Drawing.Size(100, 20);
            this.pn_pago.TabIndex = 14;
            // 
            // buscar
            // 
            this.buscar.Location = new System.Drawing.Point(311, 118);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(75, 23);
            this.buscar.TabIndex = 15;
            this.buscar.Text = "buscar";
            this.buscar.UseVisualStyleBackColor = true;
            this.buscar.Click += new System.EventHandler(this.buscar_Click);
            // 
            // modificar
            // 
            this.modificar.Location = new System.Drawing.Point(311, 196);
            this.modificar.Name = "modificar";
            this.modificar.Size = new System.Drawing.Size(75, 23);
            this.modificar.TabIndex = 16;
            this.modificar.Text = "modificar";
            this.modificar.UseVisualStyleBackColor = true;
            this.modificar.Click += new System.EventHandler(this.modificar_Click);
            // 
            // eliminar
            // 
            this.eliminar.Location = new System.Drawing.Point(311, 157);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 23);
            this.eliminar.TabIndex = 17;
            this.eliminar.Text = "eliminar";
            this.eliminar.UseVisualStyleBackColor = true;
            this.eliminar.Click += new System.EventHandler(this.eliminar_Click);
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(57, 229);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(75, 23);
            this.guardar.TabIndex = 18;
            this.guardar.Text = "guardar";
            this.guardar.UseVisualStyleBackColor = true;
            this.guardar.Click += new System.EventHandler(this.guardar_Click);
            // 
            // done
            // 
            this.done.Enabled = false;
            this.done.Location = new System.Drawing.Point(392, 196);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(75, 23);
            this.done.TabIndex = 19;
            this.done.Text = "done";
            this.done.UseVisualStyleBackColor = true;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // cerrar
            // 
            this.cerrar.Location = new System.Drawing.Point(485, 371);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(75, 23);
            this.cerrar.TabIndex = 20;
            this.cerrar.Text = "cerrar";
            this.cerrar.UseVisualStyleBackColor = true;
            this.cerrar.Click += new System.EventHandler(this.cerrar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(22, 259);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(445, 150);
            this.dataGridView1.TabIndex = 21;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 421);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cerrar);
            this.Controls.Add(this.done);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.eliminar);
            this.Controls.Add(this.modificar);
            this.Controls.Add(this.buscar);
            this.Controls.Add(this.pn_pago);
            this.Controls.Add(this.p_prestamo);
            this.Controls.Add(this.p_pago);
            this.Controls.Add(this.p_periodo);
            this.Controls.Add(this.p_interes);
            this.Controls.Add(this.p_monto);
            this.Controls.Add(this.p_codigo);
            this.Controls.Add(this.pago);
            this.Controls.Add(this.numero_prestamo);
            this.Controls.Add(this.numero_pago);
            this.Controls.Add(this.codigo_empleado);
            this.Controls.Add(this.monto);
            this.Controls.Add(this.interes);
            this.Controls.Add(this.periodos);
            this.Controls.Add(this.PAGOS);
            this.Name = "Form5";
            this.Text = "Form5";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PAGOS;
        private System.Windows.Forms.Label periodos;
        private System.Windows.Forms.Label interes;
        private System.Windows.Forms.Label monto;
        private System.Windows.Forms.Label codigo_empleado;
        private System.Windows.Forms.Label numero_pago;
        private System.Windows.Forms.Label numero_prestamo;
        private System.Windows.Forms.Label pago;
        private System.Windows.Forms.TextBox p_codigo;
        private System.Windows.Forms.TextBox p_monto;
        private System.Windows.Forms.TextBox p_interes;
        private System.Windows.Forms.TextBox p_periodo;
        private System.Windows.Forms.TextBox p_pago;
        private System.Windows.Forms.TextBox p_prestamo;
        private System.Windows.Forms.TextBox pn_pago;
        private System.Windows.Forms.Button buscar;
        private System.Windows.Forms.Button modificar;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.Button done;
        private System.Windows.Forms.Button cerrar;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}