﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace ConsoleApp1
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }

        private void entrar_Click(object sender, EventArgs e)
        {
            if (puser.Text != "" || ppass.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "select * from USUARIOS where nameu='"+puser.Text+"'and pass='"+ppass.Text+"'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("USER No Existe.");
                    puser.Text = "";
                    ppass.Text = "";
                }
                else
                {
                    Form6 admin = new Form6();
                    this.Hide();
                    admin.Closed += (s, args) => this.Close();
                    admin.Show();
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un user valido!");
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
