﻿namespace ConsoleApp1
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ABONOS = new System.Windows.Forms.Label();
            this.monto = new System.Windows.Forms.Label();
            this.codigo = new System.Windows.Forms.Label();
            this.comentario = new System.Windows.Forms.Label();
            this.tipo_cuenta = new System.Windows.Forms.Label();
            this.codigo_abono = new System.Windows.Forms.Label();
            this.p_codigo = new System.Windows.Forms.TextBox();
            this.p_coment = new System.Windows.Forms.TextBox();
            this.p_monto = new System.Windows.Forms.TextBox();
            this.p_abono = new System.Windows.Forms.TextBox();
            this.p_tipo = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.guardar = new System.Windows.Forms.Button();
            this.buscar = new System.Windows.Forms.Button();
            this.eliminar = new System.Windows.Forms.Button();
            this.modificar = new System.Windows.Forms.Button();
            this.cerrar = new System.Windows.Forms.Button();
            this.done = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // ABONOS
            // 
            this.ABONOS.AutoSize = true;
            this.ABONOS.Location = new System.Drawing.Point(207, 22);
            this.ABONOS.Name = "ABONOS";
            this.ABONOS.Size = new System.Drawing.Size(52, 13);
            this.ABONOS.TabIndex = 0;
            this.ABONOS.Text = "ABONOS";
            // 
            // monto
            // 
            this.monto.AutoSize = true;
            this.monto.Location = new System.Drawing.Point(48, 108);
            this.monto.Name = "monto";
            this.monto.Size = new System.Drawing.Size(36, 13);
            this.monto.TabIndex = 1;
            this.monto.Text = "monto";
            // 
            // codigo
            // 
            this.codigo.AutoSize = true;
            this.codigo.Location = new System.Drawing.Point(48, 75);
            this.codigo.Name = "codigo";
            this.codigo.Size = new System.Drawing.Size(39, 13);
            this.codigo.TabIndex = 2;
            this.codigo.Text = "codigo";
            // 
            // comentario
            // 
            this.comentario.AutoSize = true;
            this.comentario.Location = new System.Drawing.Point(48, 162);
            this.comentario.Name = "comentario";
            this.comentario.Size = new System.Drawing.Size(59, 13);
            this.comentario.TabIndex = 3;
            this.comentario.Text = "comentario";
            // 
            // tipo_cuenta
            // 
            this.tipo_cuenta.AutoSize = true;
            this.tipo_cuenta.Location = new System.Drawing.Point(49, 135);
            this.tipo_cuenta.Name = "tipo_cuenta";
            this.tipo_cuenta.Size = new System.Drawing.Size(63, 13);
            this.tipo_cuenta.TabIndex = 4;
            this.tipo_cuenta.Text = "tipo_cuenta";
            // 
            // codigo_abono
            // 
            this.codigo_abono.AutoSize = true;
            this.codigo_abono.Location = new System.Drawing.Point(305, 75);
            this.codigo_abono.Name = "codigo_abono";
            this.codigo_abono.Size = new System.Drawing.Size(75, 13);
            this.codigo_abono.TabIndex = 5;
            this.codigo_abono.Text = "codigo_abono";
            // 
            // p_codigo
            // 
            this.p_codigo.Location = new System.Drawing.Point(103, 75);
            this.p_codigo.Name = "p_codigo";
            this.p_codigo.Size = new System.Drawing.Size(100, 20);
            this.p_codigo.TabIndex = 6;
            // 
            // p_coment
            // 
            this.p_coment.Location = new System.Drawing.Point(113, 159);
            this.p_coment.Name = "p_coment";
            this.p_coment.Size = new System.Drawing.Size(100, 20);
            this.p_coment.TabIndex = 7;
            // 
            // p_monto
            // 
            this.p_monto.Location = new System.Drawing.Point(103, 105);
            this.p_monto.Name = "p_monto";
            this.p_monto.Size = new System.Drawing.Size(65, 20);
            this.p_monto.TabIndex = 8;
            // 
            // p_abono
            // 
            this.p_abono.Location = new System.Drawing.Point(293, 101);
            this.p_abono.Name = "p_abono";
            this.p_abono.Size = new System.Drawing.Size(100, 20);
            this.p_abono.TabIndex = 9;
            // 
            // p_tipo
            // 
            this.p_tipo.FormattingEnabled = true;
            this.p_tipo.Items.AddRange(new object[] {
            "CAP",
            "CAR"});
            this.p_tipo.Location = new System.Drawing.Point(113, 132);
            this.p_tipo.Name = "p_tipo";
            this.p_tipo.Size = new System.Drawing.Size(64, 21);
            this.p_tipo.TabIndex = 10;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(71, 224);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(322, 150);
            this.dataGridView1.TabIndex = 11;
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(52, 185);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(75, 23);
            this.guardar.TabIndex = 12;
            this.guardar.Text = "guardar";
            this.guardar.UseVisualStyleBackColor = true;
            this.guardar.Click += new System.EventHandler(this.guardar_Click);
            // 
            // buscar
            // 
            this.buscar.Location = new System.Drawing.Point(305, 127);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(75, 23);
            this.buscar.TabIndex = 13;
            this.buscar.Text = "buscar";
            this.buscar.UseVisualStyleBackColor = true;
            this.buscar.Click += new System.EventHandler(this.buscar_Click);
            // 
            // eliminar
            // 
            this.eliminar.Location = new System.Drawing.Point(305, 157);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 23);
            this.eliminar.TabIndex = 14;
            this.eliminar.Text = "eliminar";
            this.eliminar.UseVisualStyleBackColor = true;
            this.eliminar.Click += new System.EventHandler(this.eliminar_Click);
            // 
            // modificar
            // 
            this.modificar.Location = new System.Drawing.Point(305, 186);
            this.modificar.Name = "modificar";
            this.modificar.Size = new System.Drawing.Size(75, 23);
            this.modificar.TabIndex = 15;
            this.modificar.Text = "modificar";
            this.modificar.UseVisualStyleBackColor = true;
            this.modificar.Click += new System.EventHandler(this.modificar_Click);
            // 
            // cerrar
            // 
            this.cerrar.Location = new System.Drawing.Point(417, 351);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(75, 23);
            this.cerrar.TabIndex = 16;
            this.cerrar.Text = "cerrar";
            this.cerrar.UseVisualStyleBackColor = true;
            this.cerrar.Click += new System.EventHandler(this.cerrar_Click);
            // 
            // done
            // 
            this.done.Enabled = false;
            this.done.Location = new System.Drawing.Point(386, 186);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(75, 23);
            this.done.TabIndex = 17;
            this.done.Text = "done";
            this.done.UseVisualStyleBackColor = true;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 382);
            this.Controls.Add(this.done);
            this.Controls.Add(this.cerrar);
            this.Controls.Add(this.modificar);
            this.Controls.Add(this.eliminar);
            this.Controls.Add(this.buscar);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.p_tipo);
            this.Controls.Add(this.p_abono);
            this.Controls.Add(this.p_monto);
            this.Controls.Add(this.p_coment);
            this.Controls.Add(this.p_codigo);
            this.Controls.Add(this.codigo_abono);
            this.Controls.Add(this.tipo_cuenta);
            this.Controls.Add(this.comentario);
            this.Controls.Add(this.codigo);
            this.Controls.Add(this.monto);
            this.Controls.Add(this.ABONOS);
            this.Name = "Form3";
            this.Text = "Form3";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ABONOS;
        private System.Windows.Forms.Label monto;
        private System.Windows.Forms.Label codigo;
        private System.Windows.Forms.Label comentario;
        private System.Windows.Forms.Label tipo_cuenta;
        private System.Windows.Forms.Label codigo_abono;
        private System.Windows.Forms.TextBox p_codigo;
        private System.Windows.Forms.TextBox p_coment;
        private System.Windows.Forms.TextBox p_monto;
        private System.Windows.Forms.TextBox p_abono;
        private System.Windows.Forms.ComboBox p_tipo;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.Button buscar;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.Button modificar;
        private System.Windows.Forms.Button cerrar;
        private System.Windows.Forms.Button done;
    }
}