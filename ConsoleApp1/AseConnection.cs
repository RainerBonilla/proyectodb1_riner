﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Odbc;
using System.Windows.Forms;

namespace ConsoleApp1
{
    class AseConnection
    {
        public static OdbcConnection Connect()
        {
            OdbcConnection con = null;
            String conString = "Driver={Adaptive Server Enterprise};server=Oscar-PC;port=5000;db=prueba;uid=Oscar;pwd=123456";
            con = new OdbcConnection(conString);
            con.Open();
            return con;
        }
        private static void Close(OdbcConnection con)
        {
            con.Close();
        }
        public static void messageBox(string mensaje)
        {
            string caption = "Mi Cooperativa";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(mensaje, caption, buttons);
        }
    }
}
