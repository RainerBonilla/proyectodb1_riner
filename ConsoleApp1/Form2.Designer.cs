﻿namespace ConsoleApp1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cuentas = new System.Windows.Forms.Label();
            this.saldo = new System.Windows.Forms.Label();
            this.codigo = new System.Windows.Forms.Label();
            this.tipo_cuenta = new System.Windows.Forms.Label();
            this.fecha_inicio = new System.Windows.Forms.Label();
            this.guardar = new System.Windows.Forms.Button();
            this.buscar = new System.Windows.Forms.Button();
            this.eliminar = new System.Windows.Forms.Button();
            this.modificar = new System.Windows.Forms.Button();
            this.numero_cuenta = new System.Windows.Forms.Label();
            this.nuevo_saldo = new System.Windows.Forms.Label();
            this.nueva_fecha = new System.Windows.Forms.Label();
            this.cerrar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.p_codigo = new System.Windows.Forms.TextBox();
            this.p_cuenta = new System.Windows.Forms.TextBox();
            this.p_saldo = new System.Windows.Forms.TextBox();
            this.p_fecha = new System.Windows.Forms.TextBox();
            this.pn_saldo = new System.Windows.Forms.TextBox();
            this.pn_fecha = new System.Windows.Forms.TextBox();
            this.p_tipo = new System.Windows.Forms.ComboBox();
            this.done = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cuentas
            // 
            this.cuentas.AutoSize = true;
            this.cuentas.Location = new System.Drawing.Point(278, 27);
            this.cuentas.Name = "cuentas";
            this.cuentas.Size = new System.Drawing.Size(58, 13);
            this.cuentas.TabIndex = 0;
            this.cuentas.Text = "CUENTAS";
            this.cuentas.Click += new System.EventHandler(this.label1_Click);
            // 
            // saldo
            // 
            this.saldo.AutoSize = true;
            this.saldo.Location = new System.Drawing.Point(55, 111);
            this.saldo.Name = "saldo";
            this.saldo.Size = new System.Drawing.Size(32, 13);
            this.saldo.TabIndex = 1;
            this.saldo.Text = "saldo";
            // 
            // codigo
            // 
            this.codigo.AutoSize = true;
            this.codigo.Location = new System.Drawing.Point(55, 80);
            this.codigo.Name = "codigo";
            this.codigo.Size = new System.Drawing.Size(39, 13);
            this.codigo.TabIndex = 2;
            this.codigo.Text = "codigo";
            // 
            // tipo_cuenta
            // 
            this.tipo_cuenta.AutoSize = true;
            this.tipo_cuenta.Location = new System.Drawing.Point(55, 143);
            this.tipo_cuenta.Name = "tipo_cuenta";
            this.tipo_cuenta.Size = new System.Drawing.Size(63, 13);
            this.tipo_cuenta.TabIndex = 3;
            this.tipo_cuenta.Text = "tipo_cuenta";
            // 
            // fecha_inicio
            // 
            this.fecha_inicio.AutoSize = true;
            this.fecha_inicio.Location = new System.Drawing.Point(55, 170);
            this.fecha_inicio.Name = "fecha_inicio";
            this.fecha_inicio.Size = new System.Drawing.Size(64, 13);
            this.fecha_inicio.TabIndex = 4;
            this.fecha_inicio.Text = "fecha_inicio";
            // 
            // guardar
            // 
            this.guardar.Location = new System.Drawing.Point(58, 208);
            this.guardar.Name = "guardar";
            this.guardar.Size = new System.Drawing.Size(75, 23);
            this.guardar.TabIndex = 5;
            this.guardar.Text = "guardar";
            this.guardar.UseVisualStyleBackColor = true;
            this.guardar.Click += new System.EventHandler(this.guardar_Click);
            // 
            // buscar
            // 
            this.buscar.Location = new System.Drawing.Point(275, 159);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(75, 23);
            this.buscar.TabIndex = 6;
            this.buscar.Text = "buscar";
            this.buscar.UseVisualStyleBackColor = true;
            this.buscar.Click += new System.EventHandler(this.buscar_Click);
            // 
            // eliminar
            // 
            this.eliminar.Location = new System.Drawing.Point(275, 188);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 23);
            this.eliminar.TabIndex = 7;
            this.eliminar.Text = "eliminar";
            this.eliminar.UseVisualStyleBackColor = true;
            this.eliminar.Click += new System.EventHandler(this.eliminar_Click);
            // 
            // modificar
            // 
            this.modificar.Enabled = false;
            this.modificar.Location = new System.Drawing.Point(507, 208);
            this.modificar.Name = "modificar";
            this.modificar.Size = new System.Drawing.Size(75, 23);
            this.modificar.TabIndex = 8;
            this.modificar.Text = "modificar";
            this.modificar.UseVisualStyleBackColor = true;
            this.modificar.Click += new System.EventHandler(this.modificar_Click);
            // 
            // numero_cuenta
            // 
            this.numero_cuenta.AutoSize = true;
            this.numero_cuenta.Location = new System.Drawing.Point(272, 80);
            this.numero_cuenta.Name = "numero_cuenta";
            this.numero_cuenta.Size = new System.Drawing.Size(84, 13);
            this.numero_cuenta.TabIndex = 9;
            this.numero_cuenta.Text = "numero_cuenta:";
            // 
            // nuevo_saldo
            // 
            this.nuevo_saldo.AutoSize = true;
            this.nuevo_saldo.Location = new System.Drawing.Point(433, 111);
            this.nuevo_saldo.Name = "nuevo_saldo";
            this.nuevo_saldo.Size = new System.Drawing.Size(68, 13);
            this.nuevo_saldo.TabIndex = 10;
            this.nuevo_saldo.Text = "nuevo_saldo";
            // 
            // nueva_fecha
            // 
            this.nueva_fecha.AutoSize = true;
            this.nueva_fecha.Location = new System.Drawing.Point(433, 143);
            this.nueva_fecha.Name = "nueva_fecha";
            this.nueva_fecha.Size = new System.Drawing.Size(70, 13);
            this.nueva_fecha.TabIndex = 11;
            this.nueva_fecha.Text = "nueva_fecha";
            // 
            // cerrar
            // 
            this.cerrar.Location = new System.Drawing.Point(540, 386);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(75, 23);
            this.cerrar.TabIndex = 12;
            this.cerrar.Text = "cerrar";
            this.cerrar.UseVisualStyleBackColor = true;
            this.cerrar.Click += new System.EventHandler(this.cerrar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(47, 259);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(468, 150);
            this.dataGridView1.TabIndex = 13;
            // 
            // p_codigo
            // 
            this.p_codigo.Location = new System.Drawing.Point(100, 80);
            this.p_codigo.Name = "p_codigo";
            this.p_codigo.Size = new System.Drawing.Size(100, 20);
            this.p_codigo.TabIndex = 14;
            // 
            // p_cuenta
            // 
            this.p_cuenta.Location = new System.Drawing.Point(266, 104);
            this.p_cuenta.Name = "p_cuenta";
            this.p_cuenta.Size = new System.Drawing.Size(100, 20);
            this.p_cuenta.TabIndex = 15;
            // 
            // p_saldo
            // 
            this.p_saldo.Location = new System.Drawing.Point(100, 111);
            this.p_saldo.Name = "p_saldo";
            this.p_saldo.Size = new System.Drawing.Size(59, 20);
            this.p_saldo.TabIndex = 16;
            // 
            // p_fecha
            // 
            this.p_fecha.Location = new System.Drawing.Point(125, 167);
            this.p_fecha.Name = "p_fecha";
            this.p_fecha.Size = new System.Drawing.Size(100, 20);
            this.p_fecha.TabIndex = 17;
            // 
            // pn_saldo
            // 
            this.pn_saldo.Location = new System.Drawing.Point(507, 108);
            this.pn_saldo.Name = "pn_saldo";
            this.pn_saldo.Size = new System.Drawing.Size(52, 20);
            this.pn_saldo.TabIndex = 18;
            // 
            // pn_fecha
            // 
            this.pn_fecha.Location = new System.Drawing.Point(507, 143);
            this.pn_fecha.Name = "pn_fecha";
            this.pn_fecha.Size = new System.Drawing.Size(100, 20);
            this.pn_fecha.TabIndex = 19;
            // 
            // p_tipo
            // 
            this.p_tipo.FormattingEnabled = true;
            this.p_tipo.Items.AddRange(new object[] {
            "CAP",
            "CAR"});
            this.p_tipo.Location = new System.Drawing.Point(125, 140);
            this.p_tipo.Name = "p_tipo";
            this.p_tipo.Size = new System.Drawing.Size(50, 21);
            this.p_tipo.TabIndex = 20;
            // 
            // done
            // 
            this.done.Location = new System.Drawing.Point(507, 179);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(75, 23);
            this.done.TabIndex = 21;
            this.done.Text = "done";
            this.done.UseVisualStyleBackColor = true;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 421);
            this.Controls.Add(this.done);
            this.Controls.Add(this.p_tipo);
            this.Controls.Add(this.pn_fecha);
            this.Controls.Add(this.pn_saldo);
            this.Controls.Add(this.p_fecha);
            this.Controls.Add(this.p_saldo);
            this.Controls.Add(this.p_cuenta);
            this.Controls.Add(this.p_codigo);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cerrar);
            this.Controls.Add(this.nueva_fecha);
            this.Controls.Add(this.nuevo_saldo);
            this.Controls.Add(this.numero_cuenta);
            this.Controls.Add(this.modificar);
            this.Controls.Add(this.eliminar);
            this.Controls.Add(this.buscar);
            this.Controls.Add(this.guardar);
            this.Controls.Add(this.fecha_inicio);
            this.Controls.Add(this.tipo_cuenta);
            this.Controls.Add(this.codigo);
            this.Controls.Add(this.saldo);
            this.Controls.Add(this.cuentas);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cuentas;
        private System.Windows.Forms.Label saldo;
        private System.Windows.Forms.Label codigo;
        private System.Windows.Forms.Label tipo_cuenta;
        private System.Windows.Forms.Label fecha_inicio;
        private System.Windows.Forms.Button guardar;
        private System.Windows.Forms.Button buscar;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.Button modificar;
        private System.Windows.Forms.Label numero_cuenta;
        private System.Windows.Forms.Label nuevo_saldo;
        private System.Windows.Forms.Label nueva_fecha;
        private System.Windows.Forms.Button cerrar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox p_codigo;
        private System.Windows.Forms.TextBox p_cuenta;
        private System.Windows.Forms.TextBox p_saldo;
        private System.Windows.Forms.TextBox p_fecha;
        private System.Windows.Forms.TextBox pn_saldo;
        private System.Windows.Forms.TextBox pn_fecha;
        private System.Windows.Forms.ComboBox p_tipo;
        private System.Windows.Forms.Button done;
    }
}