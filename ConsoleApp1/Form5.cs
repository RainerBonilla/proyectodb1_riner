﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;

namespace ConsoleApp1
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void cerrar_Click(object sender, EventArgs e)
        {
            Form6 admin = new Form6();
            this.Hide();
            admin.Closed += (s, args) => this.Close();
            admin.Show();
        }

        private void guardar_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "" || p_monto.Text != "" || p_interes.Text != "" || p_periodo.Text!= "" || p_pago.Text != "" || p_prestamo.Text != "")
            {
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_PAGOS_CREATE @CODIGO ='" + p_codigo.Text + "',@MONTO='" + p_monto.Text + "',@INTERES='" + p_interes.Text + "',@PERIODO='" + p_periodo.Text + "',@NUMEROPAGO='"+p_pago.Text+"',@NUMEROPRE='"+p_prestamo.Text+"'";
                cmd.ExecuteNonQuery();

                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");
                p_codigo.Text = "";
                p_monto.Text = "";
                p_interes.Text = "";
                p_periodo.Text = "";
                p_pago.Text = "";
                p_prestamo.Text = "";
                guardar.Text = "";

            }

            else
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los campos del Pago!");
        }

        private void buscar_Click(object sender, EventArgs e)
        {
            if (pn_pago.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_PAGOS_READ @NUMERO ='" + pn_pago.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());


                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Pago No Existe.");
                    pn_pago.Text = "";
                }

                else
                {
                    dt.Columns["no_pago"].ColumnName = "Numero Pago";
                    dt.Columns["monto_pa"].ColumnName = "Monto";
                    dt.Columns["fecha_pa"].ColumnName = "Fecha Pago";
                    dt.Columns["capital"].ColumnName = "Capital";
                    dt.Columns["interes"].ColumnName = "Interes";
                    dt.Columns["no_prestamo"].ColumnName = "Numero Prestamo";
                    dt.Columns["codigo_emp"].ColumnName = "Codigo Empleado";
                    dataGridView1.DataSource = dt;
                    pn_pago.Text = "";
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Debe ingresar un codigo del pago");
        }

        private void eliminar_Click(object sender, EventArgs e)
        {
            if (pn_pago.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_PAGOS_READ @NUMERO ='" + pn_pago.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 1)
                {
                    ConsoleApp1.AseConnection.messageBox("Registro Eliminado Exitosamente!");
                    cmd.CommandText = "execute SP_PAGOS_DELETE @CODIGO ='" + pn_pago.Text + "'"; ;
                    cmd.ExecuteNonQuery();
                    pn_pago.Text = "";
                }
                else
                {
                    ConsoleApp1.AseConnection.messageBox("Pago No Existe!");
                    pn_pago.Text = "";
                }
            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Ingrese otro Pago!");
            }
        }

        private void modificar_Click(object sender, EventArgs e)
        {
            if (pn_pago.Text != "")
            {
                int x;
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = "execute SP_PAGOS_READ @NUMERO ='" + pn_pago.Text + "'";
                cmd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                OdbcDataAdapter da = new OdbcDataAdapter(cmd);
                da.Fill(dt);
                x = Convert.ToInt32(dt.Rows.Count.ToString());
                if (x == 0)
                {
                    ConsoleApp1.AseConnection.messageBox("Pago No Existe.");
                    pn_pago.Text = "";
                }
                else
                {
                    p_codigo.Text = dt.Rows[0]["codigo_emp"].ToString();
                    p_monto.Text = dt.Rows[0]["monto_pa"].ToString();
                    p_monto.Enabled = true;
                    p_interes.Enabled = true;
                    p_prestamo.Enabled=true;
                    p_periodo.Enabled = true;
                    p_pago.Enabled = true;
                    done.Enabled = true;
                    p_codigo.Enabled = false;
                }
            }
            else
                ConsoleApp1.AseConnection.messageBox("Ingrese un Numero de Pago!");
        }

        private void done_Click(object sender, EventArgs e)
        {
            if (p_codigo.Text != "" || p_monto.Text != "" || p_interes.Text != "" || p_periodo.Text != "" || p_pago.Text != "" || p_prestamo.Text != "")
            {
                OdbcConnection con = ConsoleApp1.AseConnection.Connect();
                OdbcCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "execute SP_PAGOS_UPDATE @NOMBRE ='" + p_codigo.Text + "',@CODIGO='"+pn_pago.Text+"',@MONTO='" + p_monto.Text + "',@INTERES='" + p_interes.Text + "',@PERIODO='" + p_periodo.Text + "',@NUMEROPAGO='" + p_pago.Text + "',@NUMEROPRE='" + p_prestamo.Text + "'";
                cmd.ExecuteNonQuery();

                ConsoleApp1.AseConnection.messageBox("Guardado Exitosamente.");
                
                p_codigo.Text = "";
                p_monto.Text = "";
                p_interes.Text = "";
                p_periodo.Text = "";
                p_pago.Text = "";
                p_prestamo.Text = "";
                guardar.Text = "";
                done.Enabled = false;
                p_codigo.Enabled = true;

            }
            else
            {
                ConsoleApp1.AseConnection.messageBox("Debe llenar todos los Campos!");
            }
        }
    }
}
