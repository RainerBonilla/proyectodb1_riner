﻿namespace ConsoleApp1
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PRESTAMOS = new System.Windows.Forms.Label();
            this.periodos = new System.Windows.Forms.Label();
            this.monto = new System.Windows.Forms.Label();
            this.saldo = new System.Windows.Forms.Label();
            this.tasa = new System.Windows.Forms.Label();
            this.codigo_emplado = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.p_codigo = new System.Windows.Forms.TextBox();
            this.p_monto = new System.Windows.Forms.TextBox();
            this.p_periodo = new System.Windows.Forms.TextBox();
            this.p_saldo = new System.Windows.Forms.TextBox();
            this.p_prestamo = new System.Windows.Forms.TextBox();
            this.pp_tasa = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.eliminar = new System.Windows.Forms.Button();
            this.modificar = new System.Windows.Forms.Button();
            this.done = new System.Windows.Forms.Button();
            this.cerrar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.p_tasa = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // PRESTAMOS
            // 
            this.PRESTAMOS.AutoSize = true;
            this.PRESTAMOS.Location = new System.Drawing.Point(222, 33);
            this.PRESTAMOS.Name = "PRESTAMOS";
            this.PRESTAMOS.Size = new System.Drawing.Size(74, 13);
            this.PRESTAMOS.TabIndex = 0;
            this.PRESTAMOS.Text = "PRESTAMOS";
            // 
            // periodos
            // 
            this.periodos.AutoSize = true;
            this.periodos.Location = new System.Drawing.Point(41, 196);
            this.periodos.Name = "periodos";
            this.periodos.Size = new System.Drawing.Size(47, 13);
            this.periodos.TabIndex = 1;
            this.periodos.Text = "periodos";
            // 
            // monto
            // 
            this.monto.AutoSize = true;
            this.monto.Location = new System.Drawing.Point(41, 164);
            this.monto.Name = "monto";
            this.monto.Size = new System.Drawing.Size(36, 13);
            this.monto.TabIndex = 2;
            this.monto.Text = "monto";
            // 
            // saldo
            // 
            this.saldo.AutoSize = true;
            this.saldo.Location = new System.Drawing.Point(41, 132);
            this.saldo.Name = "saldo";
            this.saldo.Size = new System.Drawing.Size(32, 13);
            this.saldo.TabIndex = 3;
            this.saldo.Text = "saldo";
            // 
            // tasa
            // 
            this.tasa.AutoSize = true;
            this.tasa.Location = new System.Drawing.Point(41, 100);
            this.tasa.Name = "tasa";
            this.tasa.Size = new System.Drawing.Size(27, 13);
            this.tasa.TabIndex = 4;
            this.tasa.Text = "tasa";
            // 
            // codigo_emplado
            // 
            this.codigo_emplado.AutoSize = true;
            this.codigo_emplado.Location = new System.Drawing.Point(41, 70);
            this.codigo_emplado.Name = "codigo_emplado";
            this.codigo_emplado.Size = new System.Drawing.Size(85, 13);
            this.codigo_emplado.TabIndex = 5;
            this.codigo_emplado.Text = "codigo_emplado";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(340, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "codigo_prestamo";
            // 
            // p_codigo
            // 
            this.p_codigo.Location = new System.Drawing.Point(132, 67);
            this.p_codigo.Name = "p_codigo";
            this.p_codigo.Size = new System.Drawing.Size(100, 20);
            this.p_codigo.TabIndex = 7;
            // 
            // p_monto
            // 
            this.p_monto.Location = new System.Drawing.Point(83, 161);
            this.p_monto.Name = "p_monto";
            this.p_monto.Size = new System.Drawing.Size(100, 20);
            this.p_monto.TabIndex = 8;
            // 
            // p_periodo
            // 
            this.p_periodo.Location = new System.Drawing.Point(94, 193);
            this.p_periodo.Name = "p_periodo";
            this.p_periodo.Size = new System.Drawing.Size(32, 20);
            this.p_periodo.TabIndex = 9;
            // 
            // p_saldo
            // 
            this.p_saldo.Location = new System.Drawing.Point(79, 132);
            this.p_saldo.Name = "p_saldo";
            this.p_saldo.Size = new System.Drawing.Size(100, 20);
            this.p_saldo.TabIndex = 10;
            // 
            // p_prestamo
            // 
            this.p_prestamo.Location = new System.Drawing.Point(343, 93);
            this.p_prestamo.Name = "p_prestamo";
            this.p_prestamo.Size = new System.Drawing.Size(100, 20);
            this.p_prestamo.TabIndex = 11;
            this.p_prestamo.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // pp_tasa
            // 
            this.pp_tasa.Enabled = false;
            this.pp_tasa.Location = new System.Drawing.Point(206, 98);
            this.pp_tasa.Name = "pp_tasa";
            this.pp_tasa.Size = new System.Drawing.Size(32, 20);
            this.pp_tasa.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(163, 191);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "guardar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(353, 127);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "buscar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // eliminar
            // 
            this.eliminar.Location = new System.Drawing.Point(353, 159);
            this.eliminar.Name = "eliminar";
            this.eliminar.Size = new System.Drawing.Size(75, 23);
            this.eliminar.TabIndex = 16;
            this.eliminar.Text = "eliminar";
            this.eliminar.UseVisualStyleBackColor = true;
            this.eliminar.Click += new System.EventHandler(this.eliminar_Click);
            // 
            // modificar
            // 
            this.modificar.Location = new System.Drawing.Point(353, 188);
            this.modificar.Name = "modificar";
            this.modificar.Size = new System.Drawing.Size(75, 23);
            this.modificar.TabIndex = 17;
            this.modificar.Text = "modificar";
            this.modificar.UseVisualStyleBackColor = true;
            this.modificar.Click += new System.EventHandler(this.modificar_Click);
            // 
            // done
            // 
            this.done.Enabled = false;
            this.done.Location = new System.Drawing.Point(443, 188);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(75, 23);
            this.done.TabIndex = 18;
            this.done.Text = "done";
            this.done.UseVisualStyleBackColor = true;
            this.done.Click += new System.EventHandler(this.done_Click);
            // 
            // cerrar
            // 
            this.cerrar.Location = new System.Drawing.Point(498, 363);
            this.cerrar.Name = "cerrar";
            this.cerrar.Size = new System.Drawing.Size(75, 23);
            this.cerrar.TabIndex = 19;
            this.cerrar.Text = "cerrar";
            this.cerrar.UseVisualStyleBackColor = true;
            this.cerrar.Click += new System.EventHandler(this.cerrar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(32, 236);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(432, 150);
            this.dataGridView1.TabIndex = 20;
            // 
            // p_tasa
            // 
            this.p_tasa.FormattingEnabled = true;
            this.p_tasa.Items.AddRange(new object[] {
            "Automatico",
            "Fiducario"});
            this.p_tasa.Location = new System.Drawing.Point(74, 97);
            this.p_tasa.Name = "p_tasa";
            this.p_tasa.Size = new System.Drawing.Size(121, 21);
            this.p_tasa.TabIndex = 21;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 398);
            this.Controls.Add(this.p_tasa);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cerrar);
            this.Controls.Add(this.done);
            this.Controls.Add(this.modificar);
            this.Controls.Add(this.eliminar);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pp_tasa);
            this.Controls.Add(this.p_prestamo);
            this.Controls.Add(this.p_saldo);
            this.Controls.Add(this.p_periodo);
            this.Controls.Add(this.p_monto);
            this.Controls.Add(this.p_codigo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.codigo_emplado);
            this.Controls.Add(this.tasa);
            this.Controls.Add(this.saldo);
            this.Controls.Add(this.monto);
            this.Controls.Add(this.periodos);
            this.Controls.Add(this.PRESTAMOS);
            this.Name = "Form4";
            this.Text = "Form4";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PRESTAMOS;
        private System.Windows.Forms.Label periodos;
        private System.Windows.Forms.Label monto;
        private System.Windows.Forms.Label saldo;
        private System.Windows.Forms.Label tasa;
        private System.Windows.Forms.Label codigo_emplado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox p_codigo;
        private System.Windows.Forms.TextBox p_monto;
        private System.Windows.Forms.TextBox p_periodo;
        private System.Windows.Forms.TextBox p_saldo;
        private System.Windows.Forms.TextBox p_prestamo;
        private System.Windows.Forms.TextBox pp_tasa;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button eliminar;
        private System.Windows.Forms.Button modificar;
        private System.Windows.Forms.Button done;
        private System.Windows.Forms.Button cerrar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox p_tasa;
    }
}