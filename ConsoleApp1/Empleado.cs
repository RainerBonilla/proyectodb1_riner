﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Empleado
    {
        public string primer_nombre { get; set; }
        public string segun_nombre { get; set; }
        public string primer_apellido { get; set; }
        public string segun_apellido { get; set; }
        public string fecha_naci { get; set; }
        public string calle { get; set; }
        public string avenida { get; set; }
        public int casa { get; set; }
        public string ciudad { get; set; }
        public string departamento { get; set; }
        public string referencia { get; set; }

        public Empleado(string pn,string sn, string pa, string sa, string fe, string ca, string av,int cas, string ciu, string de,string re)
        {
            this.primer_nombre = pn;
            this.segun_nombre = sn;
            this.primer_apellido = pa;
            this.segun_apellido = sa;
            this.fecha_naci = fe;
            this.calle = ca;
            this.avenida = av;
            this.casa = cas;
            this.ciudad = ciu; this.departamento = de; this.referencia = re;
        }

        public Empleado() { }
    }
}
